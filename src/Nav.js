import React from "react";
import "./App.css";
import { Link } from "react-router-dom";
function Nav() {
  const navStyle = {
    color: "white",
    textDecoration: "none"
  };
  return (
    <div>
      <nav>
        <Link style={navStyle} to="Home">
          <h3>Logo</h3>
        </Link>
        <ul className="nav-links">
          <Link style={navStyle} to="About">
            <li>About</li>
          </Link>
          <Link style={navStyle} to="Shop">
            <li>Shop</li>
          </Link>
        </ul>
      </nav>
    </div>
  );
}

export default Nav;
