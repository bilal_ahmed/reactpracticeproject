//import React, { useState } from "react";
import React from "react";
//import Tweet from "./tweet";
import "./App.css";
import Nav from "./Nav";
import About from "./About";
import Shop from "./Shop";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  /*  const [count, setCount] = useState(0);
  const [isRed, setRed] = useState(false);
  */

  //const [users] = useState([
  // { name: "Starks", message: "grey dire wolf" },
  //{ name: "Lannisters", message: "golden lion" },
  //{ name: "Boltons", message: "flayed man red on pink" }
  //]);

  /* 
  const increment = () => {
    setCount(count + 1);
    setRed(!isRed);
  };
*/

  return (
    <Router>
      <div className="App">
        {/*}   <Tweet name="Bilal" message="I'm a Software Engineer"></Tweet>
      <Tweet name="Ebad" message="I'm a Businessman"></Tweet>
      <Tweet name="Taseer" message="I'm a PHD and marketing specialist"></Tweet>
  <Tweet name="Safi" message="I'm a Student"></Tweet> 
      <h1 className={isRed ? "red" : ""}>testing states</h1>
      <button onClick={increment}>Increment</button>
      <h1>{count}</h1>{*/}
        {/*  {users.map(user => (
        <Tweet name={user.name} message={user.message} />
      ))}
    */}
        <Nav />
        <Switch>
          <Route path="/" exact component={Home}></Route>

          <Route path="/about" component={About}></Route>
          <Route path="/shop" component={Shop}></Route>
        </Switch>
      </div>
    </Router>
  );
}
const Home = () => (
  <div>
    <h1>Home Page</h1>
  </div>
);

export default App;
